## Zusammenfassung

(Fassen Sie den aufgetretenen Fehler kurz zusammen)

## Schritte zum Reproduzieren

(Wie man das Problem reproduzieren kann – das ist sehr wichtig)

## Modul, Maske, Terminal, API-Aufruf, Bericht

(In welchem Modul, Maske, Terminal, API-Aufruf bzw. Bericht tritt der Fehler auf)

## Wie ist das aktuelle Fehlerverhalten?

(Was tatsächlich passiert)

## Was ist das erwartete richtige Verhalten?

(Was Sie stattdessen erwarten)

## Relevante Log-Dateien und/oder Screenshots

(Fügen Sie alle relevaten Log-Dateien hier ein - verwenden Sie bitte code-Blöcke (```) um die Ausgabe zu formatieren, Logeinträge unr Programmcode ist sonst sehr mühsam zu lesen..)

## Mögliche Korrekturen

(Wenn Sie können, verlinken Sie auf die Codezeile, die möglicherweise für das Problem verantwortlich ist.)

/label ~bug
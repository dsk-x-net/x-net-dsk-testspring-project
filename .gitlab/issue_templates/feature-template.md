## Summary

(Summarize the requirement concisely)

## Profit/Goal

(What is the Goal of the feature - this is important to check or discuess which is the best solution for the requirement)

## Module, Window, Terminal, API-Call, Report

(In which Module, Window, Terminal, API-Call respectively Report do you expact a change)

## Detailed Explanation

(A detailed explanation what should be possible with the new feature)


## Possible solution

(If you have any idea how to solve the requirement)

/label ~feature
## Zusammenfassung

(Fassen Sie die Anforderung prägnant zusammen)

## Nutzen/Ziel

(Was ist das Ziel der Funktion - dies ist wichtig, um zu prüfen bzw. zu diskutieren, welche die beste Lösung für die Anforderung ist)

## Modul, Maske, Terminal, API-Aufruf, Bericht

(in welchem ​​Modul, Fenster, Terminal, API-Aufruf bzw. Report erwarten Sie eine Änderung)

## Ausführliche Erklärung

(Eine ausführliche Erklärung, was mit dem neuen Feature möglich sein soll)


## Mögliche Lösung

(Wenn Sie eine Idee haben, wie die Anforderung gelöst werden kann)

/label ~feature